var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var productSchema = new Schema({
  index: Number,
  dataPrzyjecia: String,
  dataOdbioru: String,
  nazwa: String,
  numerSeryjny: String,
  usterka: String,
  telefon: Number,
  wyposazenie: String,
  uwagi: String
});
mongoose.model('Product', productSchema);