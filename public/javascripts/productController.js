var app = angular.module('myApp', [
  'ngAnimate',
  'ngSanitize',
  'ngRoute',
  'mgcrea.ngStrap'
  ]);
app.controller('productController', ['$scope', '$http', '$window', '$log' , '$filter', '$alert', '$q', '$modal', function($scope, $http, $window, $log, $filter, $alert, $q, $modal) {
  $scope.init = function(){
    $scope.templateToShow = 'views/list.html';
    $scope.productsAll = [];
  	$scope.products = [];
    $scope.propertyToSort = 'index'; 
    $scope.reverse = true;
    $scope.nextHighestIndex = 0;
    $scope.productToPrint = {};
    $scope.actualEditProduct = {};
    $scope.idToDelete = '';
    getData();
  };

  $scope.sortResources = function (predicate){
          if ($scope.propertyToSort != predicate){
            $scope.propertyToSort = predicate;
            $scope.reverse = false;
          } else {
            $scope.reverse = !$scope.reverse;
          }
          $scope.products = $filter('orderBy')($scope.products, $scope.propertyToSort, $scope.reverse);
     };

  $scope.showList = function(){
    $scope.templateToShow = 'views/list.html';
  };

  $scope.print = function(product){
    $scope.productToPrint = product;
    $scope.templateToShow = 'views/podglad.html';
  };

  $scope.printIt = function(){
    var doc = new jsPDF();          
    var elementHandler = {
      '#ignorePDF': function (element, renderer) {
        return true;
      }
    };
    var source = window.document.getElementsByTagName("body")[0];
    doc.fromHTML(
        source,
        15,
        15,
        {
          'width': 180,'elementHandlers': elementHandler
        });

    doc.output("dataurlnewwindow");
  };

  var resetVars = function(){
    $scope.index = '';
    $scope.dataPrzyjecia = window.moment();
    $scope.dataOdbioru = '';
    $scope.nazwa = '';
    $scope.numerSeryjny = '';
    $scope.usterka = '';
    $scope.telefon = '';
    $scope.wyposazenie = '';
    $scope.uwagi = '';
  };

  $scope.showAddForm = function(){
    $scope.mode = 'add';
    resetVars();
    $scope.templateToShow = 'views/add.html';
  };

  $scope.showEditForm = function(product){
    $scope.actualEditProduct = product;
    $scope.mode = 'edit';
    $scope.index = product.index;
    $scope.dataPrzyjecia = window.moment(product.dataPrzyjecia, 'DD/MM/YYYY').format('DD/MM/YYYY') != 'Invalid date' ? window.moment(product.dataPrzyjecia, 'DD/MM/YYYY').toDate() : '';
    $scope.dataOdbioru = window.moment(product.dataOdbioru, 'DD/MM/YYYY').format('DD/MM/YYYY') != 'Invalid date' ? window.moment(product.dataOdbioru, 'DD/MM/YYYY').toDate() : '';
    $scope.nazwa = product.nazwa;
    $scope.numerSeryjny = product.numerSeryjny;
    $scope.usterka = product.usterka;
    $scope.telefon = product.telefon;
    $scope.wyposazenie = product.wyposazenie;
    $scope.uwagi = product.uwagi;
    $scope.templateToShow = 'views/add.html';
  };

  var findHighestIndex = function(){
    window._.each($scope.productsAll, function(product){
      $scope.nextHighestIndex = 0;
      if (product.index >= $scope.nextHighestIndex){
        $scope.nextHighestIndex = product.index + 1;
      }
    });
  };

  var getData = function(){
    var deferred = $q.defer();
  	$http.get('/list')
     .success(function(data, status, headers, config) {
        $scope.productsAll = data;
        $scope.products = data;
        $scope.products = $filter('orderBy')($scope.products, $scope.propertyToSort, $scope.reverse);
        findHighestIndex();
        $log.debug($scope.products);
        deferred.resolve( $scope.products );
      })
      .error(function(data, status, headers, config) {
        $scope.products = [];
        deferred.resolve( $scope.products );
      });
      return deferred.promise;
  };

  $scope.deleteProduct = function(id){
    $scope.idToDelete = id;
    $scope.modal = $modal({
            scope: $scope, 
            template: 'views/deleteModal.html', 
            show: true});  
  };

  $scope.hideDeleteModal = function(){
    $scope.modal.hide();
  };

  $scope.deleteProductAfterModal = function(){
    $scope.modal.hide();
    $http.post('/list/delete', {"id": $scope.idToDelete}).success(function(data, status, headers, config) {
      getData().then(function(data){
          var myAlert = $alert({
              title: 'Sprzęt został usunięty',
              container: '#alerts-container',
              type: 'success',
              show: true,
              animation: 'am-fade-and-slide-top',
              duration: 3
          });
      });    
    }).error(function(data, status, headers, config) {
         
    });
  };

  $scope.editProduct = function(){
    $log.debug('dataPrzyjecia');
    $log.debug($scope.dataPrzyjecia);
    $log.debug('moment');
    $log.debug(window.moment($scope.dataPrzyjecia));
    $log.debug(window.moment($scope.dataPrzyjecia));
    $log.debug(window.moment($scope.dataPrzyjecia));
    $log.debug(window.moment($scope.dataPrzyjecia));
    $log.debug(window.moment($scope.dataPrzyjecia));
    var toAdd = {
      index: $scope.actualEditProduct.index,
      dataPrzyjecia: window.moment($scope.dataPrzyjecia).format('DD/MM/YYYY') != 'Invalid date' ? window.moment($scope.dataPrzyjecia).format('DD/MM/YYYY'): '',
      dataOdbioru: window.moment($scope.dataOdbioru).format('DD/MM/YYYY') != 'Invalid date' ? window.moment($scope.dataOdbioru).format('DD/MM/YYYY') : '',
      nazwa: $scope.nazwa,
      numerSeryjny: $scope.numerSeryjny,
      usterka: $scope.usterka,
      telefon: $scope.telefon,
      wyposazenie: $scope.wyposazenie,
      uwagi: $scope.uwagi
    };
    $http.post('/list/edit', {"id": $scope.actualEditProduct._id, toUpdate: toAdd}).success(function(data, status, headers, config) {
      getData().then(function(data){
        $scope.showList();
          var myAlert = $alert({
              title: 'Edycja powiodła się',
              container: '#alerts-container',
              type: 'success',
              show: true,
              animation: 'am-fade-and-slide-top',
              duration: 3
          });
      });    
    }).error(function(data, status, headers, config) {
         
    });
  };

  $scope.addProduct = function(){
    var toAdd = {
      index: $scope.nextHighestIndex,
      dataPrzyjecia: window.moment($scope.dataPrzyjecia).format('DD/MM/YYYY') != 'Invalid date' ? window.moment($scope.dataPrzyjecia).format('DD/MM/YYYY'): '',
      dataOdbioru: window.moment($scope.dataOdbioru).format('DD/MM/YYYY') != 'Invalid date' ? window.moment($scope.dataOdbioru).format('DD/MM/YYYY') : '',
      nazwa: $scope.nazwa,
      numerSeryjny: $scope.numerSeryjny,
      usterka: $scope.usterka,
      telefon: $scope.telefon,
      wyposazenie: $scope.wyposazenie,
      uwagi: $scope.uwagi
    };
    $log.debug(toAdd);
    $http.post('/list/add', toAdd)
       .success(function(data, status, headers, config) {
        getData().then(function(data){
          $scope.showList();
          var myAlert = $alert({
              title: 'Sprzęt został dodany',
              container: '#alerts-container',
              type: 'success',
              show: true,
              animation: 'am-fade-and-slide-top',
              duration: 3
          });
        });
       })
       .error(function(data, status, headers, config) {
         $window.alert(data);
    });
  };

  $scope.$watch('filter', function(newVal) {
        $scope.filteredArray = $filter('filter')($scope.productsAll, {index: newVal});
        $scope.filteredArray = _.union($scope.filteredArray, $filter('filter')($scope.productsAll, {dataPrzyjecia: newVal}));
        $scope.filteredArray = _.union($scope.filteredArray, $filter('filter')($scope.productsAll, {dataOdbioru: newVal}));
        $scope.filteredArray = _.union($scope.filteredArray, $filter('filter')($scope.productsAll, {nazwa: newVal}));
        $scope.filteredArray = _.union($scope.filteredArray, $filter('filter')($scope.productsAll, {numerSeryjny: newVal}));
        $scope.filteredArray = _.union($scope.filteredArray, $filter('filter')($scope.productsAll, {usterka: newVal}));
        $scope.filteredArray = _.union($scope.filteredArray, $filter('filter')($scope.productsAll, {telefon: newVal}));
        $scope.filteredArray = _.union($scope.filteredArray, $filter('filter')($scope.productsAll, {wyposazenie: newVal}));
        $scope.filteredArray = _.union($scope.filteredArray, $filter('filter')($scope.productsAll, {uwagi: newVal}));
        $scope.products = $scope.filteredArray;
        $scope.products = $filter('orderBy')($scope.products, $scope.propertyToSort, $scope.reverse);
  });

$scope.$watch('productsAll', function(newVal) {
       if ($scope.productsAll.length == 0){
          $scope.nextHighestIndex = 0;
       }
  });

}]);