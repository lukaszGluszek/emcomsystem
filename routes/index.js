var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Product = mongoose.model('Product');
var monk = require('monk');
var db = monk('localhost:27017/serwis');
/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.get('/', function(req, res) {
	res.render('index');
});

router.get('/list', function(req, res) {
  Product.find()
  .exec(function(err, products) {
    if (!products){
      res.json(404, {msg: 'Nie znaleziono produktów.'});
    } else {
      res.json(products);
    }
});
});

router.post('/list/delete', function(req, res) {
  Product.find({ _id: req.body.id}).remove(function(err,prod){
  	res.json({msg: req.body.id});
 });
});

router.post('/list/edit', function(req, res) {
  Product.update({ _id: req.body.id }, { $set: req.body.toUpdate}, function(){
  	res.json({msg: req.body.id});
  });
});

router.post('/list/add', function(req, res) {
 var productToAdd =  new Product(req.body);
 productToAdd.save(function(err,prod){
 	res.json({msg: "Zapisano"});
 });
});

module.exports = router;
